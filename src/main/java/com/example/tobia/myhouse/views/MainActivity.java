package com.example.tobia.myhouse.views;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.tobia.myhouse.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private FloatingActionButton fabMain, fabExpenses, fabEarning;
    private boolean fabsHide = true;
    private Intent intent;
    private String nome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Carregando fragmento principal
            getSupportFragmentManager().
                    beginTransaction().
                    add(R.id.fragmentContainer, new ExpenseFormFragment()).
                    commit();

            // Carregar os FABS
            fabMain = findViewById(R.id.fabMain);
            //Chamando o método onclick dentro da sua classe
            fabMain.setOnClickListener(this);


            fabExpenses = findViewById(R.id.fabExpenses);
            fabEarning = findViewById(R.id.fabEarnings);

            //Recebendo o intent enviado pela tela de Login
            intent = getIntent();

            //Obtem os parâmetros enviados pela intent
            nome = intent.getStringExtra("email");

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);
    }


    @Override
    public void onClick(View view) {
        //Verificando se botão que chamou o onclick é o fabMain
        if (view.getId() == R.id.fabMain) {
            //A cada vez que eu clicar no FAB principal chame o método showHideFabs();
            showHideFabs();
        }
    }

    private void showHideFabs() {
        if (fabsHide) {
            fabExpenses.setVisibility(View.VISIBLE);
            fabEarning.setVisibility(View.VISIBLE);
        } else {
            fabExpenses.setVisibility(View.INVISIBLE);
            fabEarning.setVisibility(View.INVISIBLE);
        }

        fabsHide = !fabsHide;

    }
}
